import './App.css';
import DocumentSearch from './DocumentSearch';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <p>
          Welcome in Knowledge Mining browser! 
        </p>
      <DocumentSearch/>
      </header>
    </div>
  );
}

export default App;
